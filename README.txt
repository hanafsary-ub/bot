設定手順
1.抹茶SNSが利用可能であるか確認
特にMySQLにユーザー名matcha(パスワードmatcha)のアカウントでログインできるか
matchaスキーマがあるか
ついでに最新のJDKがインストールされているかも確認

2.プロジェクトの配置
適当なディレクトリにおいて
$ git clone https://hanafsary-ub@bitbucket.org/hanafsary-ub/bot.git
のコマンドでプロジェクトを配置する

3.MeCabのインストール/設定
必要なライブラリのインストール
$ sudo apt-get install libiconv-hook-dev

MeCab，MeCabのJavaバインディングのダウンロード
https://goo.gl/ZQKV12
より、
mecab-0.996.tar.gz
mecab-java-0.996.tar.gz
をダウンロードする

MeCabのインストール
$ tar xzvf mecab-0.996.tar.gz
$ cd mecab-0.996/
$ ./configure --with-charset=utf-8
$ make
$ make check
$ sudo make install

IPA辞書のダウンロード
https://goo.gl/ZQKV12

IPA辞書のインストール
$ tar xvzf mecab-ipadic-2.7.0-20070801.tar.gz
$ cd mecab-ipadic-2.7.0-20070801/
$ ./configure --with-charset=utf8
$ make
$ sudo make install

MeCabのJavaバインディングのインストール
$ tar vzxf mecab-java-0.996.tar.gz
$ cd mecab-java-0.996
MakeFileを修正する(プロジェクトフォルダに修正後のMakeFileを置いてあるので置き換える)
$ mv /抹茶SNSボットのプロジェクトディレクトリのパス/MakeFile ./MakeFile
makeの実行
$ make
MeCab.jarファイルが生成されるのでMeCab.jarをクラスパスに追加する

4.Wikipediaタイトルの辞書を追加する
/usr/local/etc/mecabrcに次の一文を追加する
userdic = /プロジェクトディレクトリまでのパス/wikipedia.dic

------------------------------------------------------
もしWikipediaの最新のキーワードを更新したいのなら
https://dumps.wikimedia.org/jawiki/latest/jawiki-latest-all-titles-in-ns0.gz
からファイルをダウンロード・解凍し、inputディレクトリに配置し
matchabot.WikipediaDictionaryを実行してwikipedia.csvを作成する。
プロジェクトのディレクトリから以下のコマンドを実行してwikipedia.dicを作成する
/usr/local/libexec/mecab/mecab-dict-index -d /usr/local/lib/mecab/dic/ipadic/ -u wikipedia.dic -f utf-8 -t utf-8 wikipedia.csv
---------------------------------------------------------------

5.プロジェクトの初期設定
プロジェクトディレクトリ/input/path.propertiesを編集する
path.directoryにmecab-java-0.996の中にあるlibMeCab.soまでの絶対パスを設定する
path.matchaに抹茶SNSのディレクトリの絶対パスを設定する

6.その他の設定
プロジェクトディレクトリにdict.sqlがあるが、これをMySQLにインポートすることにより約３万ツイート分のマルコフ辞書を導入することができる
プロジェクトディレクトリ/input/job.propertiesで定期処理の間隔を設定できる
プロジェクトディレクトリ/input/matrices.csvで問題生成元の隣接行列を設定できる
プロジェクトディレクトリ/input/twitterbot.propertiesでツイッターから学習するBotの挙動を設定できる

7.起動
起動クラスはmatchbox.JobRunnerです。
Eclipseで起動する。あるいは実行可能JARファイルを生成してJarファイルを実行するなどが考えられます。
