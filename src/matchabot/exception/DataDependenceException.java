package matchabot.exception;

/**
 * 入力隣接行列がデータ依存グラフとして正しい型でない場合にスローされる例外
 * @author 花房亮
 *
 */
public class DataDependenceException extends Exception{

	/**
	 * 自動生成されたシリアルバージョンID
	 */
	private static final long serialVersionUID = -7398089554663864155L;
	
	public DataDependenceException(String message){
		super(message);
	}

}
