package matchabot.question;

import java.util.Map;

public interface Question {

	/**
	 * 問題文を文字列で返す。
	 * @return String型の問題文
	 */
	String getText();

	/**
	 * 問題文の正解となる変数名とその変数の最終的な内容を返す。
	 * @return 変数名とその値の組み合わせのmap
	 */
	Map<String, Integer> getAnswers();

}
