package matchabot.question;

import java.util.Map;

import matchabot.graph.DataDependenceGraph;

/**
 * 文章形式のプログラム読解問題を作成する。
 * @author 花房亮
 *
 */
public class SentenceQuestion implements Question{
	private String text;
	public SentenceQuestion(DataDependenceGraph graph){
		StringBuilder sb = new StringBuilder("ある場所で基準点の0mを決め、そこから前後に1mずつ目盛り引いてあります。\n");
		sb.append("その数直線上に");
		sb.append(String.join("と", graph.getVariableNames()));
		sb.append("がいます。\n");
		
		//TODO: ソースコード部分にあたる文章と、問の部分を書く

		this.text = sb.toString();
	}
	
	@Override
	public String getText(){
		return this.text;
	}
	@Override
	public Map<String, Integer> getAnswers(){
		return null;
	}
}
