package matchabot.question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import matchabot.graph.AssignmentStatement;
import matchabot.graph.ContentPart;
import matchabot.graph.DataDependenceGraph;
import matchabot.graph.DefinitionPart;

/**
 * ソースコード形式の問題文を作成する
 * @author 花房亮
 *
 */
public class SourceCodeQuestion implements Question {
	private String text;
	private DataDependenceGraph graph;
	private Map<String, Integer> answers = new HashMap<>();
	
	public SourceCodeQuestion(DataDependenceGraph graph){
		StringBuilder sb = new StringBuilder();
		this.graph = graph;
		
		// テキスト形式の問題文の作成
		this.convertToQuestion(sb); // ソースコード形式に変換する
		sb.append("以上のプログラムの変数").append(String.join(", ", graph.getVariableNames()));
		sb.append("の最終的な内容はどんな値になるでしょう。");
		this.text = sb.toString();
		
		// プログラムから問題文の解答を抽出
		this.extractAnswers();
	}
	
	private void convertToQuestion(StringBuilder sb){
		for(AssignmentStatement line : graph.getCode()){
			sb.append(line.getDefinitionPart().getName()).append(" =");
			ContentPart cp = line.getContentPart();
			List<DefinitionPart> operands = cp.getOperands();
			List<String> operators = cp.getOperators();
			// 特に先頭の項とその周りに関して、省略できる演算子は省略する
			if(operators.get(0).equals("+") || // 先頭の+は必ず省略
			  (operators.get(0).equals("-") && operands.get(0).getName().equals("0"))) // - 0 ... の場合
			{ 
				sb.append(" ").append(operands.get(0).getName()); // 先頭の演算子を省略
			}else{// それ以外は省略しない
				sb.append(" ").append(operators.get(0)).append(operands.get(0).getName());
			}
			// １番最後の演算子と項を残して２番めの演算子から書いていく
			for(int i = 1; i < operators.size() - 1; i++ ){
				sb.append(" ").append(operators.get(i)).append(" ").append(operands.get(i).getName());
			}
			// もし最後の演算子と被演算子の組み合わせが省略できるものであったら省略する
			int lastIndex = operators.size() - 1;
			if(!((operators.get(lastIndex).equals("+") && operands.get(lastIndex).getName().equals("0")) || // ... + 0
			     (operators.get(lastIndex).equals("-") && operands.get(lastIndex).getName().equals("0")) || // ... - 0
			     (operators.get(lastIndex).equals("*") && operands.get(lastIndex).getName().equals("1")) || // ... * 1
			     (operators.get(lastIndex).equals("/") && operands.get(lastIndex).getName().equals("1")) )) // ... / 1 は省略する(それ以外の場合は省略しない)
			{
				if(operators.size() != 1){
					sb.append(" ").append(operators.get(lastIndex));
					sb.append(" ").append(operands.get(lastIndex).getName());
				}
			}
			sb.append("\n");
		}
	}
	
	// 解答となる最終的な各変数の内容をプログラムから取り出す
	private void extractAnswers(){
		for(AssignmentStatement line : graph.getCode()){
			DefinitionPart dp = line.getDefinitionPart();
			answers.put(dp.getName(), dp.getValue());
		}
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public Map<String, Integer> getAnswers(){
		return answers;
	}
}
