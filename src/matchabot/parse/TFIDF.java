package matchabot.parse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import matchabot.io.DBConnection;
import matchabot.io.TableInitializer;

/**
 * 単語のTF値とIDF値を求める
 * TF値を算出する対象の文書は抹茶SNS上のある書き込み、あるいはある会話における複数の書き込み内容とする。複数の書き込みであっても１つの文書とする。
 * IDF値における文書全体はBotの学習した全てのツイートを文書の集合とする。
 * ゆえに学習ツイート数が文書数と等しくなる
 * 
 * @author 花房亮
 *
 */
public class TFIDF {
	private List<String> nouns;
	private Map<String, Double> termFrequency;
	private Map<String, Double> documentFrequency;
	private Map<String, Double> inverseDocumentFrequency;
	private Map<String, Double> tfidf;
	
	public TFIDF(List<String> nouns){
		this.nouns = nouns;
		this.calculateTF();
		this.calculateIDF();
		this.calculateTFIDF();
	}
	
	public Map<String, Double> getTermFrequency(){
		return this.termFrequency;
	}
	
	public Map<String, Double> getInverseDocumentFrequency(){
		return this.inverseDocumentFrequency;
	}
	
	public Map<String, Double> getTFIDF(){
		return this.tfidf;
	}

	public Map<String, Double> getDocumentFrequency(){
		return this.documentFrequency;
	}

	private void calculateTF(){
		Map<String, Double> tf = new HashMap<>();
		for(String noun : nouns){
			if(tf.containsKey(noun)){
				tf.replace(noun, tf.get(noun) + 1.0);
			}else{
				tf.put(noun, 1.0);
			}
		}
		this.termFrequency = tf;
	}
	
	private void calculateIDF(){
		Map<String, Double> df = new HashMap<>();
		Map<String, Double> idf = new HashMap<>();
		double documentSize = 0.0;
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try{
			for(Map.Entry<String, Double> e : termFrequency.entrySet()){
				// TF値をDF値に加える
				df.put(e.getKey(), e.getValue());
				// T_MARKOV_TWEETから探してDF値に加算する
				PreparedStatement ps = conn.prepareStatement(
						"SELECT COUNT(*) FROM " + TableInitializer.markovTableName +
						" WHERE PREFIX1 = ?;");
				ps.setString(1, e.getKey());
				ResultSet rs = ps.executeQuery();
				if(rs.next()) df.replace(e.getKey(), df.get(e.getKey()) + rs.getInt(1));
				rs.close();
				ps.close();
				// T_MARKOV_REPLYからも探してDF値に加算する
				ps = conn.prepareStatement(
						"SELECT COUNT(*) FROM " + TableInitializer.replyMarkovTableName +
						" WHERE PREFIX1 = ?;");
				ps.setString(1, e.getKey());
				rs = ps.executeQuery();
				if(rs.next()) df.replace(e.getKey(), df.get(e.getKey()) + rs.getInt(1));
				rs.close();
				ps.close();
				// 文書数を求める
				ps = conn.prepareStatement(
						"SELECT MAX(TWEET_ID) FROM " + TableInitializer.markovTableName + ";");
				rs = ps.executeQuery();
				if(rs.next()) documentSize = rs.getInt(1);
				rs.close();
				ps.close();
				ps = conn.prepareStatement(
						"SELECT MAX(TWEET_ID) FROM " + TableInitializer.replyMarkovTableName + ";");
				rs = ps.executeQuery();
				if(rs.next()) documentSize = (documentSize < rs.getInt(1))? rs.getInt(1) : documentSize;
				rs.close();
				ps.close();
				// IDF値を求める
				idf.put(e.getKey(), Math.log(documentSize / df.get(e.getKey())) + 1.0);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnection.disconnect();
		
		this.inverseDocumentFrequency = idf;
		this.documentFrequency = df;
	}
	
	private void calculateTFIDF(){
		Map<String, Double> tfidf = new HashMap<>();
		for(Map.Entry<String, Double> e : termFrequency.entrySet()){
			tfidf.put(e.getKey(), e.getValue() / this.documentFrequency.get(e.getKey()));
		}
		this.tfidf = tfidf;
	}
	
	/*
	public static void main(String[] args){
		String message = "じゃあこれだけで良いのかというとそうではなく、雑音を排除したり、特徴量を正規化したりする必要があります。そのあたりの話はまた次回に。";
		TFIDF t = new TFIDF(MorphologicalAnalysis.extractNouns(message));
		Map<String, Double> tf = t.getTermFrequency();
		Map<String, Double> idf = t.getInverseDocumentFrequency();
		Map<String, Double> tfidf = t.getTFIDF();
		Map<String, Double> df = t.getDocumentFrequency();

		System.out.println("-------Term Frequency-----");
		for(Map.Entry<String, Double> e : tf.entrySet()){
			System.out.println(e.getKey() + " : " + e.getValue());
		}
		System.out.println("-------Document Frequency-----");
		for(Map.Entry<String, Double> e : df.entrySet()){
			System.out.println(e.getKey() + " : " + e.getValue());
		}
		System.out.println("-------Inverse Document Frequency-----");
		for(Map.Entry<String, Double> e : idf.entrySet()){
			System.out.println(e.getKey() + " : " + e.getValue());
		}
		System.out.println("-------TF-IDF-----");
		for(Map.Entry<String, Double> e : tfidf.entrySet()){
			System.out.println(e.getKey() + " : " + e.getValue());
		}
	}
	*/
}
