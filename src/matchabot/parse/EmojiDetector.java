package matchabot.parse;

/**
 * 参考:http://qiita.com/kakueki61/items/a05db9fa5075748382b8
 * @author 花房亮
 *
 */
public class EmojiDetector {
	// 絵文字が含まれているかどうか判定する
	public static boolean containsEmoji(String text){
		for(char c : text.toCharArray()){
			if(Character.isHighSurrogate(c) || Character.isLowSurrogate(c)){
				return true;
			}
		}
		return false;
	}
}
