package matchabot.parse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.chasen.mecab.Node;
import org.chasen.mecab.Tagger;

public class MorphologicalAnalysis {
	/**
	 * 形態素解析用の辞書の読み込み
	 */
	static{
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./input/path.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			File f = new File(prop.getProperty("path.dictionary"));
            System.load( f.toString() );
        } catch (UnsatisfiedLinkError e) {
        	System.err.println("Cannot load the example native code.\nMake sure your LD_LIBRARY_PATH contains \'.\'\n" + e);
            System.exit(1);
        }
	}
	/**
	 * 引数の文字列を形態素解析してArrayListに入れて渡す。最後の要素に終端文字を入れる。
	 * 
	 * @param message 形態素解析を行う対象の文字列
	 * @return 形態素解析によって品詞ごとに分けらてた文字列のリスト
	 */
	public static List<String> analyze( String message ){
		List<String> morphemes = new ArrayList<String>();
		Tagger tagger = new Tagger();
		message = removeParticularWords( message );
		// 改行ごとに分けて配列に収める
		String[] sentences = message.split( "\n" );
		for( int i = 0; i < sentences.length; i++ ){
			// 配列の中の文字列を形態素解析
			tagger.parse( sentences[i] );
			// 形態素解析したものをnodeに格納
			Node node = tagger.parseToNode( sentences[i] );
			while( node != null ){
				// 形態素の品詞などの特徴を配列に収める
				String[] features = node.getFeature().split( "," );
				// surfaceが空文字となる特殊な形態素は飛ばす(本来文頭と文末を表すもの)
				if( !features[0].equals( "BOS/EOS" ) ){
					morphemes.add( node.getSurface() );
				}
				node = node.getNext();
			}
			// 一番最後だったら終端文字、そうでなかったら改行文字を入れる。
			if( i == sentences.length - 1 ){
				morphemes.add( "[end]" );
			}else{
				// 形態素解析において改行は省略されるので置き換える必要がある
				morphemes.add( "[nextLine]" );
			}
		}
		return morphemes;
	}
	/**
	 * リプライやURLなどの特殊な文字列を除外する。
	 * 
	 * @param message
	 * @return 学習に必要のない文字列が除外された文字列
	 */
	private static String removeParticularWords( String message ){
		String processed = message;
		Pattern p = Pattern.compile( "(RT|QT) @" );
		Matcher m = p.matcher( processed );
		if( m.find() ){// リツイート文は学習しないので空文字列を格納する
			processed = "";
		}else{
			// リプライを除外
			p = Pattern.compile( "@[A-Za-z0-9_]+[\\s　]?" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
			// ハッシュタグを除外
			p = Pattern.compile( "(?:^|[^ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9&_\\/]+)[#＃]([ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9_]*[ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z]+[ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9_]*)" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
			// URLを除外
			p = Pattern.compile( "https?://[\\w/:%#\\$&\\?\\(\\)~\\.=\\+\\-]+" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
		}
		return processed;
	}

	/**
	 * 文章から名詞の属性を持つ単語のリストを抽出する
	 * @param message 解析対象の文章文字列
	 * @return 名詞の単語のリスト
	 */
	public static List<String> extractNouns(String message){
		List<String> nouns = new ArrayList<String>();
		message = removeParticularWords( message );
		Tagger tagger = new Tagger();
		tagger.parse(message);
		Node node = tagger.parseToNode(message);
		while( node != null ){
			String[] features = node.getFeature().split( "," );
			if(features[0].equals("名詞")){
				nouns.add(node.getSurface());
			}
			node = node.getNext();
		}
		return nouns;
	}
}
