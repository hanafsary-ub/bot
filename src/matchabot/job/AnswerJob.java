package matchabot.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import matchabot.io.AnswerWriter;

public class AnswerJob implements Job{
	private static final Logger log = LoggerFactory.getLogger(AnswerJob.class);

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		int questionID = AnswerWriter.answer();
		if(questionID != -1){
			log.info("問題番号:" + questionID + "の解答を書き込みました。");
		}
	}

}
