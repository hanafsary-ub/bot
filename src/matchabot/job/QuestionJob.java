package matchabot.job;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import matchabot.graph.DataDependenceGraph;
import matchabot.io.BotAccount;
import matchabot.io.MatrixLoader;
import matchabot.io.QuestionWriter;
import matchabot.question.Question;
import matchabot.question.SourceCodeQuestion;

/**
 * 抹茶SNSに定期出題するクラス。
 * 投稿間隔はBotクラスで決めている。
 * @author 花房亮
 *
 */
public class QuestionJob implements Job{

	private static final Logger log = LoggerFactory.getLogger(QuestionJob.class);
	private int botID = 0; // このBotの識別番号
	private int userID = -1; // このBotのユーザーID(USR_ID) 初期値は-1とする
	private String botName = "ソースコード問題出題ボットciri";
	private String description = "整数の代入を扱ったコードの問題を自動的に作成し提供するBotです。";
	private String path = "./input/matrices.csv";

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		if(userID == -1){
			BotAccount ba = new BotAccount(botID);
			ba.createAccount(botName, description);
			userID = ba.findUserIDFromBotTable();
		}
		List<List<String[]>> matrices = MatrixLoader.readCSV(this.path); // グラフの構造を読み込む
		List<String[]> matrix = matrices.get((int)(Math.random() * matrices.size())); // ランダムに１つ選ぶ
		Question q = new SourceCodeQuestion(new DataDependenceGraph(matrix));
		QuestionWriter.write(q, userID); // 問題文を書き込む
		log.info("\n" + q.getText());
	}
}
