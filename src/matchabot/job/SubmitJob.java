package matchabot.job;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import matchabot.io.BotAccount;
import matchabot.io.MarkovDictionary;
import matchabot.io.MessageWriter;
import matchabot.parse.MorphologicalAnalysis;
import matchabot.parse.TFIDF;
import matchabot.twitter.TwitterBot;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Twitterから学習する人工無脳Botの自発的な投稿を定期的にするJob
 * twitterbot.propertiesにおいて指定された確率で独り言を話すか、誰かに話しかけるか決まる
 * 誰かに話しかける場合、このBotのアカウントをフォローしている人で、誰にも返信されていない書き込みに対して返信する。
 * @author 花房亮
 *
 */
public class SubmitJob extends TwitterBot implements Job{
	private static final Logger log = LoggerFactory.getLogger(SubmitJob.class);
	private static int userID = -1; // このBotのユーザーID(USR_ID) 初期値は-1とする
	private static int maxLength; // 生成する文章の最大文字数
	private static int maxRetry;  // 生成する文章が最大文字数を超えていた場合、再生成を試行する回数
	private static double talkingRate; // 誰かに話しかける確率
	
	static{
		BotAccount ba = new BotAccount(botID);
		ba.createAccount(botName, description);
		userID = ba.findUserIDFromBotTable();

		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./input/twitterbot.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		maxLength = Integer.parseInt(prop.getProperty("generate.max.length"));
		maxRetry = Integer.parseInt(prop.getProperty("generate.max.retry"));
		talkingRate = Double.parseDouble(prop.getProperty("talking.rate"));
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		MessageWriter mw = new MessageWriter(userID);
		List<Integer> targets = mw.findTargets();
		
		// 返信する対象があれば一定の確率で誰かに話しかける
		if(!targets.isEmpty() && Math.random() < talkingRate){
			// 返信対象を求める
			int targetID = targets.get((int)(Math.random() * targets.size()));
			// 返信対象の書き込み内容を取得する
			String message = mw.fetchMessage(targetID);
			// 書き込み内容を学習する(TF-IDF値が最も高い単語が辞書にない場合を防ぐため)
			if(!MarkovDictionary.detectSpam(message)){
				MarkovDictionary.learn(MorphologicalAnalysis.analyze(message), MarkovDictionary.TABLE_TYPE_TWEET);
			}
			// TF-IDF値を求め、最も高い単語から文章を生成する
			List<String> nouns = MorphologicalAnalysis.extractNouns(message); // 文章から名詞のリストを求める
			String sendMessage = "";
			if(!nouns.isEmpty()){
				Map<String, Double> tfidf = new TFIDF(nouns).getTFIDF();
				// TF-IDF値の最も高い名詞を求める
				double max = 0.0;
				String maxNoun = "";
				for(Map.Entry<String, Double> e : tfidf.entrySet()){
					if(e.getValue() > max){
						max = e.getValue();
						maxNoun = e.getKey();
					}
				}
				// maxNounを含む単語からマルコフ連鎖で文章を作成する
				sendMessage = MarkovDictionary.generateMessage(maxNoun);
				// 最大文字数を超えていたら再生成する
				for(int i = 0; sendMessage.length() > maxLength && i < maxRetry; i++){
					sendMessage = MarkovDictionary.generateMessage(maxNoun);
				}
			}else{ // 返信先の書き込みに名詞が含まれなかった場合
				// 通常のマルコフ連鎖で文章を作成する
				sendMessage = MarkovDictionary.generateMessage();
				// 最大文字数を超えていたら再生成する
				for(int i = 0; sendMessage.length() > maxLength && i < maxRetry; i++){
					sendMessage = MarkovDictionary.generateMessage();
				}
			}
			if(sendMessage.length() <= maxLength){
				mw.write(sendMessage, targetID);
				log.info("返信:\n" + sendMessage);
			}else{
				log.info(maxRetry + "回再生成しましたが、" + maxLength + "文字以内の文章を生成することができませんでした。");
			}
		}else{ // 誰かに話しかけるのではなく、独立した投稿をする場合
			// 通常のマルコフ連鎖で文章を作成する
			String sendMessage = MarkovDictionary.generateMessage();
			// 最大文字数を超えていたら再生成する
			for(int i = 0; sendMessage.length() > maxLength && i < maxRetry; i++){
				sendMessage = MarkovDictionary.generateMessage();
			}
			if(sendMessage.length() <= maxLength){
				mw.write(sendMessage);
				log.info("独り言:\n" + sendMessage);
			}else{
				log.info(maxRetry + "回再生成しましたが、" + maxLength + "文字以内の文章を生成することができませんでした。");
			}
		}
	}

}
