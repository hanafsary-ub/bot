package matchabot.job;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import matchabot.io.BotAccount;
import matchabot.io.MarkovDictionary;
import matchabot.io.ReplyMessageWriter;
import matchabot.parse.MorphologicalAnalysis;
import matchabot.parse.TFIDF;
import matchabot.twitter.TwitterBot;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplyJob extends TwitterBot implements Job {
	private static final Logger log = LoggerFactory.getLogger(ReplyJob.class);
	private static int userID;
	private static int contextLimit; // TF-IDFで考慮する書き込みの数
	private static int maxLength; // 生成する文章の最大文字数
	private static int maxRetry;  // 生成する文章が最大文字数を超えていた場合、再生成を試行する回数

	static{
		BotAccount ba = new BotAccount(botID);
		ba.createAccount(botName, description);
		userID = ba.findUserIDFromBotTable();
		
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./input/twitterbot.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		contextLimit = Integer.parseInt(prop.getProperty("context.limit"));
		maxLength = Integer.parseInt(prop.getProperty("generate.max.length"));
		maxRetry = Integer.parseInt(prop.getProperty("generate.max.retry"));
	}
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ReplyMessageWriter rmw = new ReplyMessageWriter(userID);
		List<Integer> targets = rmw.findTargets();
		targets.stream().forEach(s -> System.out.println(s));
		// 返信対象があれば返信する
		if(!targets.isEmpty()){
			int replyTo = targets.get((int)(Math.random() * targets.size()));
			List<String> messages = rmw.fetchMessages(replyTo, contextLimit);
			StringBuilder builder = new StringBuilder();
			// 書き込み内容を学習する(TF-IDF値が最も高い単語が辞書にない場合を防ぐため)
			for(String mes : messages){
				if(!MarkovDictionary.detectSpam(mes)){
					MarkovDictionary.learn(MorphologicalAnalysis.analyze(mes), MarkovDictionary.TABLE_TYPE_REPLY);
					builder.append(mes);
				}
			}
			// TF-IDF値を求め、最も高い単語から文章を生成する
			List<String> nouns = MorphologicalAnalysis.extractNouns(builder.toString()); // 文章から名詞のリストを求める
			String sendMessage = "";

			if(!nouns.isEmpty()){
				Map<String, Double> tfidf = new TFIDF(nouns).getTFIDF();
				// TF-IDF値の最も高い名詞を求める
				double max = 0.0;
				String maxNoun = "";
				for(Map.Entry<String, Double> e : tfidf.entrySet()){
					if(e.getValue() > max){
						max = e.getValue();
						maxNoun = e.getKey();
					}
				}
				// maxNounを含む単語からマルコフ連鎖で文章を作成する
				sendMessage = MarkovDictionary.generateMessage(maxNoun);
				// 最大文字数を超えていたら再生成する
				for(int i = 0; sendMessage.length() > maxLength && i < maxRetry; i++){
					sendMessage = MarkovDictionary.generateMessage(maxNoun);
				}
			}else{ // 返信先の書き込みに名詞が含まれなかった場合
				// 通常のマルコフ連鎖で文章を作成する
				sendMessage = MarkovDictionary.generateMessage();
				// 最大文字数を超えていたら再生成する
				for(int i = 0; sendMessage.length() > maxLength && i < maxRetry; i++){
					sendMessage = MarkovDictionary.generateMessage();
				}
			}
			if(sendMessage.length() <= maxLength){
				rmw.write(sendMessage, replyTo);
				log.info("返信:" + replyTo + "\n" + sendMessage);
			}else{
				log.info(maxRetry + "回再生成しましたが、" + maxLength + "文字以内の文章を生成することができませんでした。");
			}
		}

	}

}
