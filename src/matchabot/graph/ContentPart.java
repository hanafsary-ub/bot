package matchabot.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * 代入文X = YのYの部分
 * 整数の多項式で構成される
 * @author 花房亮
 *
 */
public class ContentPart {
	private List<DefinitionPart> operands = new ArrayList<>(); // 被演算子。代入式の項
	private List<String> operators = new ArrayList<>(); // 演算子
	private int result; // 計算結果。つまり代入内容
	
	/**
	 * 代入内容を計算する
	 * このメソッドを呼び出す前に被演算子operandsと演算子operatorsを設定する必要がある
	 * @return 演算子と被演算子が設定されていなければfalseを返す
	 */
	public boolean calculate(){
		boolean calculable = false;
		List<DefinitionPart> tempOperands = new ArrayList<>(operands);
		List<String> tempOperators = new ArrayList<>(operators);
		if(!(this.operands.isEmpty() || this.operators.isEmpty())){
			calculable = true;
			// 計算優先度の高い*, /, %から計算する(計算して１つの変数にまとめる)
			for(int i = 1; i < tempOperators.size(); i++){ // 最初の演算子は必ず+か-なので飛ばす
				String op = tempOperators.get(i);
				DefinitionPart dp = null;
				if(op.equals("*") || op.equals("/") || op.equals("%")){
					if(op.equals("*")){
						dp = new DefinitionPart(tempOperands.get(i).getName(), tempOperands.get(i - 1).getValue() * tempOperands.get(i).getValue());
					}
					else if(op.equals("/")){
						dp = new DefinitionPart(tempOperands.get(i).getName(), (int)(tempOperands.get(i - 1).getValue() / tempOperands.get(i).getValue()));
					}
					else if(op.equals("%")){
						dp = new DefinitionPart(tempOperands.get(i).getName(), tempOperands.get(i - 1).getValue() % tempOperands.get(i).getValue());
					}
					tempOperands.set(i, dp);
					// i番目に計算を置き換えたあとはi-1番目の被演算子とi番目の演算子を取り除く。インクリメントはしない
					tempOperators.remove(i);
					tempOperands.remove(i - 1);
					i--;
				}
			}
			this.result = 0;
			// 残った演算子+と-、そして被演算子による計算結果を求める
			for(int i = 0; i < tempOperators.size(); i++){
				if(tempOperators.get(i).equals("+")){
					this.result += tempOperands.get(i).getValue();
				}
				else{// -の場合
					this.result -= tempOperands.get(i).getValue();
				}
			}
		}
		return calculable;
	}
	public int getResult(){
		return this.result;
	}
	public List<DefinitionPart> getOperands(){
		return this.operands;
	}
	public List<String> getOperators(){
		return this.operators;
	}
}
