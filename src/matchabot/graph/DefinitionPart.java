package matchabot.graph;

/**
 * 代入文X = YのXの部分
 * 変数名と変数の持つ整数値を扱う
 * @author 花房亮
 *
 */
public class DefinitionPart {
	private String name; // 変数名
	private int value; // 変数の持つ整数値
	
	public DefinitionPart(String name){
		this.name = name;
	}
	public DefinitionPart(String name, int value){
		this.name = name;
		this.value = value;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setValue(int value){
		this.value = value;
	}
	public String getName(){
		return this.name;
	}
	public int getValue(){
		return this.value;
	}
}
