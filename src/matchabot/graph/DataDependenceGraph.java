package matchabot.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import matchabot.exception.DataDependenceException;
import matchabot.io.MatrixLoader;

/**
 * 代入文のみで構成されるプログラムの構造
 * 隣接行列から自動的にコードを生成する
 * @author 花房亮
 *
 */
public class DataDependenceGraph {
	private List<AssignmentStatement> code; // 代入文のリスト。すなわちプログラムソースコード
	private List<String[]> matrix; // 入力の隣接行列
	private List<String> variableNames = new ArrayList<>(); // プログラムで使用する変数のリスト
	
	public DataDependenceGraph(List<String[]> matrix){
		try {
			this.matrix = MatrixLoader.checkDataDependenceException(matrix);
		} catch (DataDependenceException e) {
			e.printStackTrace();
		}
		this.generateCode();
	}
	
	public List<AssignmentStatement> getCode(){
		return this.code;
	}
	public List<String[]> getAdjacencyMatrix(){
		return this.matrix;
	}
	public List<String> getVariableNames(){
		return this.variableNames;
	}

	// 隣接行列からプログラムソースコードを生成する
	private void generateCode(){
		this.code = new ArrayList<>();
		int variableCount = 0; // 変数の数を数え上げる
		for(int i = 0; i < this.matrix.size(); i++){

			AssignmentStatement statement = new AssignmentStatement(i);
			/*変数定義部を決定する*/
			variableCount = this.assignTemporaryName(statement, variableCount); // 仮の変数名を決めておく
			/*定義内容部を決定する*/
			ContentPart cp = new ContentPart();
			this.assignOperands(cp, i);           // 被演算子を決める
			Collections.shuffle(cp.getOperands()); // DefinitionContentの項の順番をランダムに入れ替える
			this.assignOperators(cp);             // 演算子を決める
			statement.setContentPart(cp);

			/*ノードiの変数定義部の値を計算する*/
			cp.calculate();
			statement.getDefinitionPart().setValue(cp.getResult());

			this.code.add(statement); 

		}
		//ランダムな変数名を割り当てる。定数の名前を数値に置き換える
		this.assignNames(variableCount);
	}
	
	// 仮の変数名を割り当てる
	private int assignTemporaryName(AssignmentStatement statement, int count){
		List<Integer> reuseableID = new ArrayList<>();
		int lineNumber = statement.getLineNumber();
		// i行目の代入文の変数定義部の変数名がlineNumberの代入文の変数名として代用できるかどうか確認する
		for(int i = 0; i <= lineNumber - 1; i++){
			// もし再利用可能なら変数名が再利用可能な行として記憶しておく
			if(this.isReuseable(lineNumber, i)){
				reuseableID.add(i);
			}
		}
		
		int size = reuseableID.size();
		// もし再利用可能な変数名が存在すれば再利用する
		if(size > 0){
			int reuseID = reuseableID.get(((int)(Math.random() * size))); // 再利用可能なものの中からランダムに選ぶ
			// 変数名を再利用する
			String reuseName = this.code.get(reuseID).getDefinitionPart().getName();
			statement.setDefinitionPart(new DefinitionPart(reuseName));
		}else{
			// 仮の変数名をつける
			statement.setDefinitionPart(new DefinitionPart(Integer.toString(count)));
			// 再利用しないので使用する変数の数をカウントアップする
			count++;
		}
		return count;
	}

	// targetIDの行の代入文の変数名をresuseIDの行の代入文の変数名に置き換えることができるかどうか
	private boolean isReuseable(int targetID, int reuseID){
		boolean reuseable = true;
		if(targetID != this.matrix.size() - 1){ // targetIDが最後の行であれば必ず変数名の再利用が可能
			for(int i = targetID + 1; i < this.matrix.size(); i++){
				int e = Integer.parseInt(this.matrix.get(reuseID)[i]);
				if(e == 1){
					reuseable = false;
					break;
				}
			}
		}
		return reuseable;
	}
	
	// lineNumber行の参照する変数を代入文の定義内容部に追加し、定数をランダムに決める
	private void assignOperands(ContentPart content, int lineNumber){
		for(int i = 0; i < this.matrix.size(); i++){
			//ノードiについて隣接行列を列方向に見る
			int element = Integer.parseInt(this.matrix.get(i)[lineNumber]);
			if(element == 1){ // 1ならj番目の変数を変数定義部に追加する
				content.getOperands().add(this.code.get(i).getDefinitionPart());
			}
		}
		// 定数の追加
		DefinitionPart constant = new DefinitionPart("CONSTANT");
		// 定数の値は0から9ランダム
		constant.setValue((int)(Math.random() * 10));
		content.getOperands().add(constant);
	}
	
	// 演算子をランダムに決める
	private void assignOperators(ContentPart content){
		List<String> operators = content.getOperators();
		List<DefinitionPart> operands = content.getOperands();
		for(int i = 0; i < operands.size(); i++){
			/* とりあえず和と差のみで実装する
			 * コメントアウトを解除すれば乗除剰余にも対応可能(ただし文章化の変換アルゴリズムを拡張しなければならない)
			 * 
			// 選択可能な演算子のリスト。状況に応じて使えない演算子は除外して、残った演算子の中からランダムに選ぶ
			// 除外する理由は被験者に必要以上の暗算の負荷を与えたくないため。
			// 除外理由の詳細 → https://www.evernote.com/l/ABxPMk20NzNEvJjaPHtrfhUoxderPJCuRdU
			List<String> op = new ArrayList<>(Arrays.asList("+", "-", "*", "/", "%"));
			if(i == 0){//最初の項なら+か-以外を除外する
				op.remove("*");
				op.remove("/");
				op.remove("%");
			}
			else{// 最初の項でない場合
				// 一つ前の演算子が*, /, %のいずれかだったら次は必ず+か-でなければならない
				if(operators.get(i-1).equals("*") || operators.get(i-1).equals("/") || operators.get(i-1).equals("%")){
					op.remove("*");
					op.remove("/");
					op.remove("%");
				}else{// 1つ前が+か-だった場合
					// 2番目の項で最初の項が-だったら余りを計算しない。
					if(i == 1 && operators.get(0).equals("-")){
						op.remove("%");
					}
					// 掛け算は一桁同士でなければ選択されない
					if(operands.get(i-1).getValue() > 9 || operands.get(i).getValue() > 9){
						op.remove("*");
					}
					if(operands.get(i).getValue() == 0){//ゼロ除算が起きる場合は'/'と'%'を除外する
						op.remove("/");
						op.remove("%");
					}
					else{// ゼロ除算が起きない
						//割り切れない場合除算は選択されない
						if(operands.get(i-1).getValue() % operands.get(i).getValue() != 0){
							op.remove("/");
						}
						// 余りは1より大きい数同士でなければならない。さらに先の項が後の項よりも大きいか等しくなければならない。
						if(operands.get(i-1).getValue() <= 0 || operands.get(i).getValue() <= 0 ||
								operands.get(i-1).getValue() < operands.get(i).getValue()){
							op.remove("%");
						}
					}
				}
			}
			*/
			// とりあえず和と差のみで実装する
			List<String> op = new ArrayList<>(Arrays.asList("+", "-"));
			// 選択可能な演算子の中から１つランダムに選ぶ
			int r = (int)(Math.random() * op.size());
			operators.add(op.get(r));
		}
	}
	
	// 仮の変数名を正式な変数名に置き換える
	private void assignNames(int count){
		VariableNameGenerator generator = new VariableNameGenerator(count);
		for(int i = 0; i < count; i++){
			String name = generator.assign();
			this.variableNames.add(name);
			for(int j = 0; j < this.code.size(); j++){
				DefinitionPart dp = this.code.get(j).getDefinitionPart();
				if(dp.getName().equals(Integer.toString(i))){
					dp.setName(name);
				}
			}
			
		}
		// 定数の名前を数値に置き換える
		for(int i = 0; i < this.code.size(); i++){
			List<DefinitionPart> operands = this.code.get(i).getContentPart().getOperands();
			for(DefinitionPart dp : operands){
				if(dp.getName().equals("CONSTANT")){
					dp.setName(Integer.toString(dp.getValue()));
				}
			}
		}
		
	}
}
