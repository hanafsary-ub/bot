package matchabot.graph;

/**
 * プログラムソースコードの代入文
 * @author 花房亮
 *
 */
public class AssignmentStatement {
	private int lineNumber; // 行番号
	private DefinitionPart definition; // 代入文の変数が定義される部分。左辺
	private ContentPart content; // 代入文の変数の内容を決める多項式の部分。右辺
	
	public AssignmentStatement(int lineNumber){
		this.lineNumber = lineNumber;
	}
	public AssignmentStatement(int lineNumber, DefinitionPart definition, ContentPart content){
		this.lineNumber = lineNumber;
		this.definition = definition;
		this.content = content;
	}
	
	public void setDefinitionPart(DefinitionPart definition){
		this.definition = definition;
	}
	public void setContentPart(ContentPart content){
		this.content = content;
	}
	public int getLineNumber(){
		return this.lineNumber;
	}
	public DefinitionPart getDefinitionPart(){
		return this.definition;
	}
	public ContentPart getContentPart(){
		return this.content;
	}
}
