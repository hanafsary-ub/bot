package matchabot.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MessageWriter {
	protected int userID;

	public MessageWriter(int userID){
		this.userID = userID;
	}

	public void write(String message){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
            String sql = "INSERT INTO T_TIME_LINE(ACT_ID, USR_ID, MESSAGE, INSERT_DATE) VALUES(?, ?, ?, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, 1);
			ps.setInt(2, this.userID);
			ps.setString(3, message);
			ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate(); // insert文の実行
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
	}

	public void write(String message, int targetID){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
            String sql = "INSERT INTO T_TIME_LINE(ACT_ID, VAL_ID, USR_ID, MESSAGE, INSERT_DATE) VALUES(?, ?, ?, ?, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, 2);
			ps.setInt(2, targetID);
			ps.setInt(3, this.userID);
			ps.setString(4, message);
			ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate(); // insert文の実行
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
	}
	
	/**
	 * botをフォローしてるユーザーの(リプライメッセージでない)投稿の中で誰からもメッセージの来てない投稿を探す
	 * @return botをフォローしてるユーザーの(リプライメッセージでない)投稿の中で誰からもメッセージの来てない投稿のID
	 */
	public List<Integer> findTargets(){
		List<Integer> targets = new ArrayList<>();
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			PreparedStatement ps1 = conn.prepareStatement("SELECT USR_ID FROM T_FRIEND WHERE F_USR_ID = ?;");
			ps1.setInt(1, userID);
			ResultSet followers = ps1.executeQuery();

			// botをフォローしてるユーザーを探す
			while(followers.next()){
				PreparedStatement ps2 = conn.prepareStatement(
						"SELECT TML_ID FROM T_TIME_LINE WHERE USR_ID = ? AND ACT_ID = 1 AND DEL_FLG = 0;");
				ps2.setInt(1, followers.getInt(1));
				ResultSet submit = ps2.executeQuery();
				
				// フォローしてるユーザーの返信メッセージでない書き込みを探す
				while(submit.next()){
					PreparedStatement ps3 = conn.prepareStatement(
							"SELECT TML_ID FROM T_TIME_LINE WHERE VAL_ID = ?;");
					ps3.setInt(1, submit.getInt(1));
					ResultSet reply = ps3.executeQuery();
					// そのメッセージに返信が一件もついてなかったら候補に入れる
					if(!reply.next()){
						targets.add(submit.getInt(1));
					}
					reply.close();
					ps3.close();
				}
				submit.close();
				ps2.close();
			}
			followers.close();
			ps1.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();

		return targets;
	}
	
	/**
	 * timelineIDに該当する書き込み内容をT_TIME_LINEテーブルから取得する
	 * @param timelineID T_TIME_LINEにおけるTML_ID
	 * @return T_TIME_LINEにおけるMESSAGE, 例外の発生あるいは該当するデータがない場合nullが返される。
	 */
	public String fetchMessage(int timelineID){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT MESSAGE FROM T_TIME_LINE WHERE TML_ID = " + timelineID + ";");
			if(rs.next()) return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		return null;
	}
}
