package matchabot.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import matchabot.parse.MorphologicalAnalysis;

public class AnswerWriter {

	public static int answer(){
		int answeredQuestionID = -1;
		
		DBConnection.connect();
		try{
			if(TableInitializer.existTable(TableInitializer.questionTableName) &&
					TableInitializer.existTable(TableInitializer.answerTableName)){
				
                Connection conn = DBConnection.getConnection();
                Statement stmt = conn.createStatement();
                String sql = "SELECT * FROM " + TableInitializer.questionTableName + " WHERE ANSWERER <=> NULL;";
                ResultSet rs = stmt.executeQuery(sql);
				
				while(rs.next()){
					int answererID = findAnswerer(rs.getInt(2));
					int userID     = rs.getInt(3); // 出題したBotのユーザーID(USR_ID)
					int timelineID = rs.getInt(2); // 問題のもつTML_ID
					int questionID = rs.getInt(1); // QST_ID

					// 未回答の問題に対して回答している書き込みを探す
					if(answererID != -1){
						// 回答者がいたのならその問題を解答済みの問題とする
						answeredQuestionID = questionID;
						// T_BOT_QUESTIONのANSWERERを回答者のユーザIDで更新する
						registerAnswerer(answererID, questionID);
						// 回答者の回答内容と正解不正解を記録する
						int correct = registerReplyAnswerInfo(timelineID, questionID);
						// 解答を書き込む
						writeAnswer(createAnswerMessage(questionID, answererID, correct), userID, timelineID);
					}
				}
				rs.close();
				stmt.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
		return answeredQuestionID; // 解答を書き込んだ問題番号を返す。-1なら回答者は見つかっていない
	}

	// 戻り値は1なら正解0なら不正解
	private static int registerReplyAnswerInfo(int timelineID, int questionID) throws SQLException{
		int correct = 0;
		Connection conn = DBConnection.getConnection();
    	// 未回答の問題のTML_IDから、その問題に対して答えている書き込みを探す
	    String sql = "SELECT TML_ID, MESSAGE FROM T_TIME_LINE WHERE VAL_ID = " + timelineID + ";";
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(sql);
    	if(rs.next()){
    		int ansTimelineID = rs.getInt(1);
    		String message = rs.getString(2);
    		// 回答を形態素解析し正解しているかどうか調べる
    		correct = (isCorrect(MorphologicalAnalysis.analyze(message), questionID))? 1 : 0;
    		TableInitializer.createReplyAnswerTableIfNeeded();
    		sql = "INSERT INTO " + TableInitializer.replyAnswerTableName + "(TML_ID, IS_CORRECT, QST_ID) VALUES(?, ?, ?);";
    		PreparedStatement ps = conn.prepareStatement(sql);
    		ps.setInt(1, ansTimelineID);
    		ps.setInt(2, correct);
    		ps.setInt(3, questionID);
    		ps.execute();
    		ps.close();
    	}
    	rs.close();
    	stmt.close();
    	return correct;
	}
	
	private static boolean isCorrect(List<String> morphs, int questionID) throws SQLException{
		boolean correct = true;
		Connection conn = DBConnection.getConnection();
	    Statement stmt = conn.createStatement();
	    String sql = "SELECT * FROM " + TableInitializer.answerTableName + " WHERE QST_ID = " + questionID + ";";
	    ResultSet rs = stmt.executeQuery(sql);
	    int questionCount = 0; // 解答の変数の数
	    int answerCount = 0;   // 回答の変数の数
	    while(rs.next()){
	    	questionCount++;
	    	String varName = rs.getString("VARIABLE"); // 解答の変数名
	    	int value = rs.getInt("VALUE");            // 解答の変数名に対応する正解
	    	boolean appear = false; // 変数名が登場したか
	    	boolean minus = false;  // ハイフンが登場したか
	    	for(String word : morphs){
	    		// もし変数名と同じキーワードを見つけら次にくる数字が回答した変数の内容だと予測できる
	    		if(word.equals(varName)){
	    			appear = true;
	    			continue;
	    		}
	    		// 既に変数名が出現済みか
	    		if(appear){
	    			// この単語は整数の数字か
	    			if(isNumber(word)){
	    				int op = (minus)? -1 : 1;
	    				answerCount++;
	    				// 解答と異なればfalseに
	    				if(value != op * Integer.parseInt(word)) correct = false;
	    				break;
	    			}
	    			else if(word.equals("-")){ // -も形態素で分解されてしまうので-を見つけたら数字がでた時に-を掛けるようにする
	    				minus = true;
	    			}
	    		}
	    	}
	    }
	    // 回答の変数の数が足りなくても不正解とする
	    if(questionCount != answerCount) correct = false;
	    rs.close();
	    stmt.close();

		return correct;
	}
	private static boolean isNumber(String val){
		try{
			Integer.parseInt(val);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}

	// 未回答の問題に回答者がいるか探す
	// いれば回答者のユーザーIDを返す。いなければ-1を返す
	private static int findAnswerer(int timelineID) throws SQLException{
		Connection conn = DBConnection.getConnection();
	    String sql = "SELECT * FROM T_TIME_LINE WHERE VAL_ID = ?;";
    	PreparedStatement ps = conn.prepareStatement(sql);
    	ps.setInt(1, timelineID); // 未回答の問題のTML_IDから、その問題に対して答えている書き込みを探す
    	ResultSet rs = ps.executeQuery();
    	int ret = (rs.next())? rs.getInt(3) : -1;
    	rs.close();
    	ps.close();
    	return ret;
	}

	// T_QUESTION_ANSWERテーブルからquestionIDの解答を探し、その解答を用いて解答用の文章を作成する
	private static String createAnswerMessage(int questionID, int answererID, int correct) throws SQLException{

		Connection conn = DBConnection.getConnection();
	    Statement stmt = conn.createStatement();
	    String sql1 = "SELECT NAME FROM T_USER WHERE USR_ID = " + answererID + ";";
		ResultSet rs1 = stmt.executeQuery(sql1);
		String answererName = (rs1.next())? rs1.getString(1) : "(名称未設定)";
		rs1.close();
		String sql = "SELECT * FROM T_QUESTION_ANSWER WHERE QST_ID = " + questionID + ";";
		ResultSet rs = stmt.executeQuery(sql);
		
		StringBuilder sb = new StringBuilder(answererName);
		sb.append("さんが回答権を獲得しました。");
		if(correct == 1){
			sb.append("正解です。\n");
		}else{
			sb.append("不正解です。\n");
		}
		sb.append("正解は、\n");
		while(rs.next()){
			sb.append(rs.getString(2)) // 変数名
				.append(" = ")
				.append(rs.getInt(3)) // 値
				.append("\n"); 
		}
		sb.append("です。");

		return sb.toString();
	}

	private static void writeAnswer(String answerMessage, int userID, int replyTo) throws SQLException{
		Connection conn = DBConnection.getConnection();
		String sql = "INSERT INTO T_TIME_LINE(ACT_ID, USR_ID, VAL_ID, MESSAGE, INSERT_DATE, DEL_FLG) VALUES(?, ?, ?, ?, ?, ?);";
		PreparedStatement ps = conn.prepareStatement(sql);
		// アクションID(ACT_ID)の決定 アクションIDは通常メッセージだと1で、返信メッセージだと2をとる
		ps.setInt(1, 2);
		// ユーザーID
		ps.setInt(2, userID);
		// 返信先のタイムラインID(VAL_ID) ※返信先は回答者の書き込みではない
		ps.setInt(3, replyTo);
		// メッセージ内容(MESSAGE)
		ps.setString(4, answerMessage);
		// INSERT_DATEの決定
		ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
		// DEL_FLGは0
		ps.setInt(6,  0);
		ps.execute(); // insert文の実行
		ps.close();
	}
	
	// 回答者のユーザーIDを問題テーブルに記録する
	private static void registerAnswerer(int answererID, int questionID) throws SQLException{
		Connection conn = DBConnection.getConnection();
		String sql = "UPDATE T_BOT_QUESTION SET ANSWERER = ? WHERE QST_ID = ?;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, answererID);
		ps.setInt(2, questionID);
		ps.executeUpdate();
		ps.close();
	}
}
