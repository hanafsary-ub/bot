package matchabot.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import matchabot.exception.DataDependenceException;

public class MatrixLoader {
	
	/**
	 * CSVファイルから入力用の行列を読み込む
	 * @param path 入力ファイルのあるパス
	 * @return 行列のリスト
	 */
	public static List<List<String[]>> readCSV(String path){
		List<List<String[]>> matrices = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			List<String[]> m = new ArrayList<>();
			String line = "";
			while((line = br.readLine()) != null){
				// 行頭の文字が'-'だったら行列に切り変える
				if(line.charAt(0) == '-'){
					matrices.add(m);
					m = new ArrayList<>();
				}else{
					m.add(line.split(","));
				}
			}
			if(!m.isEmpty()){ // 最後の行が'-'を含む行でなかったら最後の行列を追加する
				matrices.add(m);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return matrices;
	}
	
	public static List<String[]> checkDataDependenceException(List<String[]> matrix) throws DataDependenceException{
		if(matrix.size() < 2){
			throw new DataDependenceException("隣接行列の大きさは2x2以上でなければなりません。");
		}
		for(int i = 0; i < matrix.size(); i++){
			String[] line = matrix.get(i);
			if(line.length != matrix.size()){
				throw new DataDependenceException("行列が正方行列ではありません。");
			}
			
			for(int j = 0; j < line.length; j++){
				int e = Integer.parseInt(line[j]);
				if(!(e == 0 || e == 1)){
					throw new DataDependenceException("隣接行列の要素に0または1でない成分が含まれています。");
				}
				// データ依存グラフの隣接行列は対角成分と下三角成分がすべて0でなければならない
				if(j <= i && e != 0){
					throw new DataDependenceException("対角成分および下三角成分に0でないものが含まれています。");
				}
			}
		}
		// 例外がなければそのまま返すことになる
		return matrix;
	}
}
