package matchabot.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * MySQLのmatchaデータベースアクセス用クラス。
 * 使い方:
 * public void sample(){
 *     // 接続
 *     DBConnection.connect();
 *     // Connectionの取得
 *     Connection conn = DBConnection.getConnection();
 *     // sql文の実行
 *     conn.createStatement().execute("SELECT * FROM T_USER;");
 *     // 切断
 *     DBConnection.disconnect();
 *     
 * @author 花房亮
 *
 */
public class DBConnection {

    private final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private final static String USER_NAME = "matcha";
	private final static String PASSWORD = "matcha";
	private final static String DB_URL = "jdbc:mysql://localhost/matcha";
	private static Connection conn = null;
	
	/**
	 * データベースに接続します。
	 */
	public static void connect(){
		// JDBCドライバを読み込む
		try {
			Class.forName( JDBC_DRIVER ).newInstance();
			// 接続
			Properties prop = new Properties();
			prop.put( "user",  USER_NAME);
			prop.put( "password", PASSWORD );
			prop.put( "characterEncoding", "utf8" );
			conn = DriverManager.getConnection( DB_URL, prop );
		}catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e){
			e.printStackTrace();
			if( conn != null ){
				try {
					conn.rollback();
					conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * データベースから切断します。
	 */
	public static void disconnect(){
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if( conn != null ){
				try {
					conn.rollback();
					conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * {@link java.sql.Connection}を返します。
	 * 
	 * @return {@link java.sql.Connection}
	 */
	public static Connection getConnection(){
		return conn;
	}
}