package matchabot.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Map;

import matchabot.question.Question;

public class QuestionWriter {

	public static void write(Question question, int userID){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
            String sql = "INSERT INTO T_TIME_LINE(ACT_ID, USR_ID, MESSAGE, INSERT_DATE) VALUES(?, ?, ?, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			//ps.setInt(1, tmlID);
			// アクションID(ACT_ID)の決定 アクションIDは通常メッセージだと1で、返信メッセージだと2をとる
			ps.setInt(1, 1);
			// ユーザーID
			ps.setInt(2, userID);
			// メッセージ内容(MESSAGE)
			ps.setString(3, question.getText());
			// INSERT_DATEの決定
			ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps.execute(); // insert文の実行
			ps.close();
			// タイムラインID(TML_ID)の決定
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT TML_ID FROM T_TIME_LINE ORDER BY TML_ID DESC LIMIT 1;");
			int tmlID = 0;
			if(rs.next()) tmlID = rs.getInt(1);
			rs.close();
			
			registerQuestion(tmlID, userID); // 問題文用テーブルに書き込みデータを登録する
			registerAnswer(question.getAnswers());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
	}

	// 問題を作成し、タイムラインに投稿する
	private static void registerQuestion(int timelineID, int userID){
		Connection conn = DBConnection.getConnection();
		try {
			// 問題文保存用テーブルが作成されていなかったら作成する
			TableInitializer.createQuestionTableIfNeeded();

			String sql = "INSERT INTO " + TableInitializer.questionTableName + "(TML_ID, BOT_ID) VALUES(?, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, timelineID);
			ps.setInt(2, userID);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static void registerAnswer(Map<String, Integer> answers){
		Connection conn = DBConnection.getConnection();
		try {
			// 問題に対する解答のテーブルがなければ作成する
			TableInitializer.createAnswerTableIfNeeded();

			String select = "SELECT QST_ID FROM " + TableInitializer.questionTableName + " ORDER BY QST_ID DESC LIMIT 1;";
			String insert = "INSERT INTO " + TableInitializer.answerTableName + "(VARIABLE, VALUE, QST_ID) VALUES(?, ?, ?);";

			// 問題番号の取得
			ResultSet rs = conn.createStatement().executeQuery(select);
			int questionID = (rs.next())? rs.getInt(1) : -1;

			// 解答を書き込む
			PreparedStatement ps = conn.prepareStatement(insert);
			for(Map.Entry<String, Integer> e : answers.entrySet()){
				ps.setString(1, e.getKey());
				ps.setInt(2, e.getValue());
				ps.setInt(3, questionID);
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
