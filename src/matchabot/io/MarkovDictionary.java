package matchabot.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarkovDictionary {
	public static final int TABLE_TYPE_TWEET = 0;
	public static final int TABLE_TYPE_REPLY = 1;
	private static final int spamThreshold = 1;

	/**
	 * 形態素解析で分ち書きされた文章をマルコフ辞書に学習する。
	 * 
	 * @param morphs 分ち書きされた文節のリスト
	 * @param type 学習する文章がリプライとして書かれたのかそうでないか
	 * @return 今回学習する文章が累計何ツイート目のものか
	 */
	public static int learn(List<String> morphs, int type){
		int tweetCount = 0;

		DBConnection.connect();
		Connection conn = DBConnection.getConnection();

		try {
			Statement stmt = conn.createStatement();
			// tweetCountを求める
			ResultSet rsTweet = stmt.executeQuery("SELECT MAX(TWEET_ID) FROM " + TableInitializer.markovTableName + ";");
			int maxIDTweet = (rsTweet.next())? rsTweet.getInt(1) : 0;
			rsTweet.close();
			ResultSet rsReply = stmt.executeQuery("SELECT MAX(TWEET_ID) FROM " + TableInitializer.replyMarkovTableName + ";");
			int maxIDReply = (rsReply.next())? rsReply.getInt(1) : 0;
			rsReply.close();
			stmt.close();
			tweetCount = (maxIDTweet < maxIDReply)? maxIDReply + 1 : maxIDTweet + 1; // 何個目のツイートを学習するのか

			// どのテーブルに挿入するか
			String insertTo = "";
			if(type == TABLE_TYPE_TWEET){
				insertTo = TableInitializer.markovTableName;
			}else{
				insertTo = TableInitializer.replyMarkovTableName;
			}

			PreparedStatement ps = conn.prepareStatement(
					"INSERT INTO " + insertTo + "(TWEET_ID, PREFIX1, PREFIX2, SUFFIX, IS_HEAD) VALUES(?,?,?,?,?);");
			int i = 0;
			String suf;
			do{
				// 文頭であればheadを1そうでなければ0とする
				int head = ( i == 0 )? 1: 0;
				String pre1 = (String)( morphs.get( i++ ) );
				// prefix1が[end]なら、つまり元の文章が空文字列なら学習をせずに終了する
				if( pre1.equals( "[end]" ) ) break;

				String pre2 = (String)( morphs.get( i++ ) );
				// prefix2に[end]が来たら代わりに[empty]を入れ、suffixに[end]を入れる
				if( pre2.equals( "[end]" ) ){
					pre2 = "[empty]";
					suf = "[end]";
				}else{
					suf  = (String)( morphs.get( i-- ) );
				}
				// 挿入データの決定
				ps.setInt( 1, tweetCount ); ps.setString( 2, pre1 ); ps.setString( 3, pre2 ); ps.setString( 4, suf ); ps.setInt( 5, head );
				ps.addBatch();
				
			}while(!suf.equals("[end]"));
			// addBatchでまとめておいた挿入の実行
			ps.executeBatch();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnection.disconnect();

		return tweetCount;
	}
	
	/**
	 * 文章の頭を適当に選んでケツ([end]タグ)に当たるまでに適当に文を繋げていく。
	 * 完成した文章を返す
	 * @return マルコフ辞書を元に生成された文章
	 */
	public static String generateMessage(){
		StringBuilder builder = new StringBuilder();
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();

		try {
			Statement stmt = conn.createStatement();
			// 文頭属性を持つ要素から生成を開始する
			ResultSet rs = stmt.executeQuery("SELECT PREFIX1, PREFIX2, SUFFIX FROM " + TableInitializer.markovTableName + " WHERE IS_HEAD = 1 ORDER BY MAR_ID DESC LIMIT 1000;");
			if(rs.last()){
				// 候補の中からランダムに決めるために候補の数を求める
				int size = rs.getRow();// ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
				rs.absolute((int)(Math.random() * size) + 1);
				
				// URLのみのツイートなど空文字列が学習された場合prefix1に[end]が入る場合がある
				if(rs.getString("PREFIX1").equals("[end]")) return "";
				
				if(rs.getString("PREFIX2").equals("[empty]")){
					builder.append(rs.getString("PREFIX1"));
				}else{
					builder.append(rs.getString("PREFIX1"))
						   .append(rs.getString("PREFIX2"));
				}
				
				PreparedStatement ps = conn.prepareStatement(
						"SELECT PREFIX1, PREFIX2, SUFFIX FROM " + TableInitializer.markovTableName
						+ " WHERE PREFIX1 = ? AND PREFIX2 = ? ORDER BY MAR_ID DESC LIMIT 1000;",
						ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
						ResultSet.CONCUR_READ_ONLY);

				// 続きがある場合
				while(!rs.getString("SUFFIX").equals("[end]")){
					builder.append(rs.getString("SUFFIX"));
					
					// 次に繋がる単語を探す
					ps.setString(1, rs.getString("PREFIX2"));
					ps.setString(2, rs.getString("SUFFIX"));
					
					rs.close();
					rs = ps.executeQuery();
					// 候補の中からランダムに決めるために候補の数を求める
					rs.last();              // ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
					size = rs.getRow();
					rs.absolute((int)(Math.random() * size) + 1);
				}
				rs.close();
				ps.close();
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnection.disconnect();
		
		// [nextLine]タグを改行文字に変換して返す
		String message = builder.toString();
		Pattern p = Pattern.compile("\\[nextLine\\]");
		Matcher m = p.matcher(message);
		return m.replaceAll("\n");
	}

	public static String generateMessage(String startingWord){
		StringBuilder builder = new StringBuilder();
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();

		try {
			// 特定のキーワードからマルコフ連鎖を繋げていく
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.replyMarkovTableName + " WHERE PREFIX1 = ? ORDER BY MAR_ID DESC LIMIT 1000;",
						ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
						ResultSet.CONCUR_READ_ONLY);
			ps.setString(1, startingWord);
			ResultSet rs = ps.executeQuery();
			String prefix1 = "", prefix2 = "", suffix = "";
			boolean head = false;
			boolean reply = true; // 特定のキーワードがT_MARKOV_REPLYにあったか
			if(rs.next()){
				// 候補の中からランダムに決めるために候補の数を求める
				rs.last();              // ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
				int size = rs.getRow();
				rs.absolute((int)(Math.random() * size) + 1);

				prefix1 = rs.getString("PREFIX1");
				prefix2 = rs.getString("PREFIX2");
				suffix = rs.getString("SUFFIX");
				head = (rs.getInt("IS_HEAD") == 1)? true : false;
			}else{// T_MARKOV_REPLYから見つからなかった場合はT_MARKOV_TWEETから探す
				reply = false;
				rs.close(); ps.close();
				ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.markovTableName + " WHERE PREFIX1 = ? ORDER BY MAR_ID DESC LIMIT 1000;",
						ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
						ResultSet.CONCUR_READ_ONLY);
				ps.setString(1, startingWord);
				rs = ps.executeQuery();
				if(rs.next()){
					// 候補の中からランダムに決めるために候補の数を求める
					rs.last();              // ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
					int size = rs.getRow();
					rs.absolute((int)(Math.random() * size) + 1);

					prefix1 = rs.getString("PREFIX1");
					prefix2 = rs.getString("PREFIX2");
					suffix = rs.getString("SUFFIX");
					head = (rs.getInt("IS_HEAD") == 1)? true : false;
				}
				rs.close(); ps.close();
			}
			if(prefix2.equals("[empty]")){
				builder.append(prefix1);
			}else{
				builder.append(prefix1).append(prefix2);
			}

			String b_prefix1 = prefix1;
			String b_prefix2 = prefix2;
			boolean b_head = head;
			// head属性が見つかるまで前方に進めていく
			while(!b_head){
				if(reply){
					ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.replyMarkovTableName + " WHERE PREFIX2 = ? AND SUFFIX = ? ORDER BY MAR_ID DESC LIMIT 1000;",
							ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
							ResultSet.CONCUR_READ_ONLY);
				}else{
					ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.markovTableName + " WHERE PREFIX2 = ? AND SUFFIX = ? ORDER BY MAR_ID DESC LIMIT 1000;",
							ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
							ResultSet.CONCUR_READ_ONLY);
				}
				ps.setString(1, b_prefix1);
				ps.setString(2, b_prefix2);
				rs = ps.executeQuery();
				// 候補の中からランダムに決めるために候補の数を求める
				rs.last();              // ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
				int size = rs.getRow();
				rs.absolute((int)(Math.random() * size) + 1);
				
				b_prefix1 = rs.getString("PREFIX1");
				b_prefix2 = rs.getString("PREFIX2");
				b_head = (rs.getInt("IS_HEAD") == 1)? true : false;
				builder.insert(0, b_prefix1);
				rs.close(); ps.close();
			}
			// [end]属性が見つかるまで後方に進めていく
			while(!suffix.equals("[end]")){
				builder.append(suffix);
				if(reply){
					ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.replyMarkovTableName + " WHERE PREFIX1 = ? AND PREFIX2 = ? ORDER BY MAR_ID DESC LIMIT 1000;",
							ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
							ResultSet.CONCUR_READ_ONLY);
				}else{
					ps = conn.prepareStatement("SELECT * FROM " + TableInitializer.markovTableName + " WHERE PREFIX1 = ? AND PREFIX2 = ? ORDER BY MAR_ID DESC LIMIT 1000;",
							ResultSet.TYPE_SCROLL_INSENSITIVE,      // rs.absolute(int)で順方向以外にも辿れるようにする
							ResultSet.CONCUR_READ_ONLY);
				}
				ps.setString(1, prefix2);
				ps.setString(2, suffix);
				rs = ps.executeQuery();
				// 候補の中からランダムに決めるために候補の数を求める
				rs.last();              // ResultSetは1から数えるため最後の行番号がResultSetの大きさに等しい
				int size = rs.getRow();
				rs.absolute((int)(Math.random() * size) + 1);
				
				prefix2 = rs.getString("PREFIX2");
				suffix = rs.getString("SUFFIX");
				rs.close(); ps.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnection.disconnect();

		// [nextLine]タグを改行文字に変換して返す
		String message = builder.toString();
		Pattern p = Pattern.compile("\\[nextLine\\]");
		Matcher m = p.matcher(message);
		return m.replaceAll("\n");
	}
	
	/**
	 * ツイート内容がスパムかどうか判定する
	 * @param message ツイート
	 * @return trueならスパムfalseならスパムでない
	 */
	public static boolean detectSpam(String message){
		boolean detect = false;
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT COUNT(*) FROM " + TableInitializer.tweetTableName + " WHERE TWEET = ?;");
			ps.setString(1, message);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				if(rs.getInt(1) >= spamThreshold){
					detect = true;
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
		return detect;
	}
	
	/**
	 * スパム検出用にツイートそのままを保存する
	 * @param message ツイート内容
	 */
	public static void saveTweet(String message){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try{
			PreparedStatement ps = conn.prepareStatement("INSERT INTO " + TableInitializer.tweetTableName + "(TWEET) VALUES(?);");
			ps.setString(1, message);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		DBConnection.disconnect();
		
	}
	
	/*
	public static void main(String[] args){
		for(int i = 0; i < 10; i++){
			System.out.println(MarkovDictionary.generateMessage("映画"));
			System.out.println("-------------------------------");
		}
	}
	*/
}
