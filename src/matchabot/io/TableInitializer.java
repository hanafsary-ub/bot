package matchabot.io;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TableInitializer {
	public static final String botTableName = "T_BOT";
	public static final String questionTableName = "T_BOT_QUESTION";
	public static final String answerTableName = "T_QUESTION_ANSWER";
	public static final String replyAnswerTableName = "T_USER_ANSWER";
	public static final String markovTableName = "T_MARKOV_TWEET";
	public static final String replyMarkovTableName = "T_MARKOV_REPLY";
	public static final String tweetTableName = "T_LEARNED_TWEET";

	// 指定したテーブルが存在するかどうか返す
	public static boolean existTable(String tableName) throws SQLException{
		boolean exist = false;

		Connection conn = DBConnection.getConnection();
		DatabaseMetaData dmd = conn.getMetaData();
		String[] types = {"TABLE"};
		// 指定のテーブルが作成済みかどうか
		ResultSet rs = dmd.getTables(null, "matcha", tableName, types);
		exist = rs.next();
		rs.close();

		return exist;
	}
	
	public static void createMarkovTableIfNeeded(){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			Statement statement = conn.createStatement();
			if(!existTable(markovTableName)){
				statement.executeUpdate(
						"CREATE TABLE " + markovTableName
						+ "(MAR_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "TWEET_ID INT NOT NULL, "
						+ "PREFIX1 VARCHAR(255) NOT NULL, "
						+ "PREFIX2 VARCHAR(255) NOT NULL, "
						+ "SUFFIX VARCHAR(255) NOT NULL, "
						+ "IS_HEAD TINYINT(1) NOT NULL);");
			}
			if(!existTable(replyMarkovTableName)){
				statement.executeUpdate(
						"CREATE TABLE " + replyMarkovTableName
						+ "(MAR_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "TWEET_ID INT NOT NULL, "
						+ "PREFIX1 VARCHAR(255) NOT NULL, "
						+ "PREFIX2 VARCHAR(255) NOT NULL, "
						+ "SUFFIX VARCHAR(255) NOT NULL, "
						+ "IS_HEAD TINYINT(1) NOT NULL);");
			}
			if(!existTable(tweetTableName)){
				statement.executeUpdate(
						"CREATE TABLE " + tweetTableName
						+ "(TWT_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "TWEET TEXT NOT NULL);");
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
	}

	public static void createBotTableIfNeeded(){
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			if(!existTable(botTableName)){
				Statement statement = conn.createStatement();
				String sql = "CREATE TABLE " + botTableName 
						+ "(BOT_ID INT NOT NULL PRIMARY KEY, "
						+ "USR_ID INT NOT NULL, "
						+ "DESCRIPTION TEXT NOT NULL); ";
				statement.executeUpdate(sql);
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
	}

	public static void createQuestionTableIfNeeded(){
		Connection conn = DBConnection.getConnection();
		try {
			// 問題文保存用テーブルが作成されていなかったら作成する
			if(!existTable(questionTableName)){
				Statement statement = conn.createStatement();
				String sql = "CREATE TABLE " + questionTableName 
						+ "(QST_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "TML_ID INT NOT NULL, "
						+ "BOT_ID INT NOT NULL, "
						+ "ANSWERER INT DEFAULT NULL);";
				statement.executeUpdate(sql);
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void createAnswerTableIfNeeded(){
		Connection conn = DBConnection.getConnection();
		try {
			// 問題に対する解答のテーブルがなければ作成する
			if(!existTable(answerTableName)){
				Statement statement = conn.createStatement();
				String sql = "CREATE TABLE " + answerTableName
						+ "(ANS_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "VARIABLE VARCHAR(255) NOT NULL, "
						+ "VALUE INT NOT NULL, "
						+ "QST_ID INT NOT NULL);";
				statement.executeUpdate(sql);
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void createReplyAnswerTableIfNeeded(){
		Connection conn = DBConnection.getConnection();
		try {
			if(!existTable(replyAnswerTableName)){
				Statement statement = conn.createStatement();
				String sql = "CREATE TABLE " + replyAnswerTableName
						+ "(ANS_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, "
						+ "TML_ID INT NOT NULL, "
						+ "IS_CORRECT TINYINT(1) NOT NULL, "
						+ "QST_ID INT NOT NULL);";
				statement.executeUpdate(sql);
				statement.close();
				System.out.println("回答テーブルを作成しました。");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
