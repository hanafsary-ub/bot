package matchabot.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Properties;

/**
 * Botのユーザーアカウントとの紐付けをデータベースで管理する
 * @author 花房亮
 *
 */
public class BotAccount {
	
	private int botID;
	
	public BotAccount(int botID){
		this.botID = botID;
	}

	/**
	 * 定期的に投稿をするBotの抹茶SNSアカウントを作成します。
	 * T_BOTテーブルにも新規にBotを登録します。
	 * 既に登録済みのBotの場合何もしません。
	 * @param name 抹茶SNS上に表示するBotの名前
	 * @param description Botの説明
	 */
	public void createAccount(String name, String description){
		
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			PreparedStatement ps = conn.prepareStatement(
				"INSERT INTO T_USER(NAME,MAIL,DIRECTORY1,DIRECTORY2,THUMBNAIL,DESCRIPTION,AUTHORITY,STATUS,INSERT_DATE) VALUES(?,?,?,?,?,?,?,?,?);");
			ps.setString(1, name);
			ps.setString(2, "bot." + Integer.toString(botID) + "@hoge.com");
			ps.setString(3, "00000");
			ps.setString(4, "bot" + botID);
			ps.setString(5, "default.jpg");
			ps.setString(6, description);
			ps.setInt(7, 0);
			ps.setInt(8, 1);
			ps.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
			// T_BOTに同じbotIDがなければ作成する
			if(!existBot()){
				createDirectory(); // botのアカウントのサムネイル画像などを保存するディレクトリを作成する
				ps.execute();
				registerBotTable(description); // botテーブルにbotの情報を登録する
			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
	}
	
	public int findUserIDFromBotTable(){
		int userID = -1;
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT USR_ID FROM " + TableInitializer.botTableName + " WHERE BOT_ID = " + botID + ";");
			if(rs.next()){
				userID = rs.getInt(1);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		return userID;
	}
	
	private void createDirectory(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./input/path.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		StringBuilder botRoot = new StringBuilder(prop.getProperty("path.matcha"));
		if(!botRoot.toString().endsWith("/")) botRoot.append("/");
		botRoot.append("app/files/user/00000/bot").append(botID);
		// フォルダの作成
		StringBuilder storage = new StringBuilder(botRoot.toString()).append("/storage");
		new File(storage.toString() ).mkdirs();
		File f = new File(botRoot.append("/thumbnail").toString());
		f.mkdirs();
		// 画像ファイルのコピー
		Path source1 = new File("./input/default_images/original").toPath();
		Path source2 = new File("./input/default_images/preview").toPath();
		Path source3 = new File("./input/default_images/thumbnail").toPath();
		Path target1 = new File(f.toString() + "/original").toPath();
		Path target2 = new File(f.toString() + "/preview").toPath();
		Path target3 = new File(f.toString() + "/thumbnail").toPath();
		try {
			Files.copy(source1, target1, StandardCopyOption.REPLACE_EXISTING);
			Files.copy(source2, target2, StandardCopyOption.REPLACE_EXISTING);
			Files.copy(source3, target3, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean existBot(){
		boolean exist = false;
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM " + TableInitializer.botTableName + " WHERE BOT_ID = " + botID + ";");
			exist = rs.next();
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return exist;
	}
	
	private void registerBotTable(String description){
		Connection conn = DBConnection.getConnection();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT USR_ID FROM T_USER ORDER BY USR_ID DESC LIMIT 1;");
			int userID = -1;
			if(rs.next()){
				userID = rs.getInt(1);
			}
			PreparedStatement ps = conn.prepareStatement( "INSERT INTO T_BOT VALUES(?,?,?);");
			ps.setInt(1, botID);
			ps.setInt(2, userID);
			ps.setString(3, description);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
