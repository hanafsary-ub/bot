package matchabot.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReplyMessageWriter extends MessageWriter{

	public ReplyMessageWriter(int userID) {
		super(userID);
	}
	
	/**
	 * このbotが個別投稿、返信したことのある投稿の中で最新の返信がこのbotによるものでない投稿を探す
	 * @return 返信対象の候補リスト
	 */
	@Override
	public List<Integer> findTargets(){
		List<Integer> targets = new ArrayList<>();
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		try {
			PreparedStatement ps1 = conn.prepareStatement("SELECT TML_ID FROM T_TIME_LINE WHERE USR_ID = ? AND ACT_ID = 1");
			ps1.setInt(1, userID);
			ResultSet rs1 = ps1.executeQuery();
			// botの返信でない書き込みへの返信を探す
			while(rs1.next()){
				int timelineID = rs1.getInt(1);
                PreparedStatement ps2 = conn.prepareStatement(
                		"SELECT USR_ID FROM T_TIME_LINE WHERE VAL_ID = ? AND DEL_FLG = 0 ORDER BY TML_ID DESC LIMIT 1;");
                ps2.setInt(1, timelineID);
                ResultSet rs2 = ps2.executeQuery();
                // 返信があり、最新の返信がbot自身のものでなければbotの書き込みのTML_IDを返信先候補のリストに加える
                if(rs2.next() && rs2.getInt(1) != userID){
                	targets.add(timelineID);
                }
                rs2.close(); ps2.close();
			}
			rs1.close(); ps1.close();

			ps1 = conn.prepareStatement("SELECT DISTINCT VAL_ID FROM T_TIME_LINE WHERE USR_ID = ? AND ACT_ID = 2");
			ps1.setInt(1, userID);
			rs1 = ps1.executeQuery();
			// botが返信した返信先について
			while(rs1.next()){
                PreparedStatement ps2 = conn.prepareStatement(
                		"SELECT TML_ID, USR_ID FROM T_TIME_LINE WHERE TML_ID = ? AND DEL_FLG = 0;");
                ps2.setInt(1, rs1.getInt(1));
                ResultSet rs2 = ps2.executeQuery();
                // 返信先の書き込みがbotの書き込みでなければ,その書き込みに返信が付いているか調べる
                if(rs2.next() && rs2.getInt("USR_ID") != userID){
                	int timelineID = rs2.getInt("TML_ID");
                	PreparedStatement ps3 = conn.prepareStatement(
                		"SELECT USR_ID FROM T_TIME_LINE WHERE VAL_ID = ? AND DEL_FLG = 0 ORDER BY TML_ID DESC LIMIT 1;");
                	ps3.setInt(1, timelineID);
                	ResultSet rs3 = ps3.executeQuery();
                	// 最新の返信がbot自身のものでなければTML_IDを返信先候補のリストに加える
                	if(rs3.next() && rs3.getInt("USR_ID") != userID){
                		targets.add(timelineID);
                	}
                	rs3.close(); ps3.close();
                }
				rs2.close(); ps2.close();
			}
			rs1.close(); ps1.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();

		return targets;
	}
	
	/**
	 * timelineIDの書き込みのスレッド内の最新limit件の書き込み内容を結合した文字列を取得する。
	 * たとえばある書き込みに対する返信が3件ある場合に、
	 * limit = 2の場合、最新の返信２件の書き込み内容の文字列のリストを返す。
	 * limit = 3の場合、返信すべての書き込み内容の文字列のリストを返す。
	 * limit = 4の場合、返信元の書き込みとそれに対する返信、計4件の書き込み内容の文字列のリストを返す。
	 * limit = 5以上の場合でもlimit = 4と同じものが返ってくる。
	 * limit = 0だと空のリストを返す
	 * limit = -1だと会話に関する全ての書き込みの文字列のリストを返す
	 * @param timelineID 返信でない投稿のTML_ID
	 * @param limit 最新何件までの結果を返すか
	 * @return 該当する書き込み内容の文字列のリスト
	 */
	public List<String> fetchMessages(int timelineID, int limit){
		List<String> messages = new ArrayList<>();
		DBConnection.connect();
		Connection conn = DBConnection.getConnection();
		
		String sql = "SELECT MESSAGE FROM T_TIME_LINE WHERE VAL_ID = ? OR TML_ID = ? AND DEL_FLG = 0 ORDER BY TML_ID DESC LIMIT ?;";
		String noLimitSQL = "SELECT MESSAGE FROM T_TIME_LINE WHERE VAL_ID = ? OR TML_ID = ? AND DEL_FLG = 0 ORDER BY TML_ID DESC;";
		if(limit == -1) sql = noLimitSQL;
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, timelineID);
			ps.setInt(2, timelineID);
			if(limit != -1) ps.setInt(3, limit);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				messages.add(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnection.disconnect();
		
		return messages;
	}
}
