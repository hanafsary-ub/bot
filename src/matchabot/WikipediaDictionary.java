package matchabot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikipediaDictionary {
	private static String inputPath = "./input/jawiki-latest-all-titles-in-ns0";
	private static String outputPath = "./wikipedia.csv";
	public static void main(String[] args) {
		System.out.println(inputPath + "をmecabの形式に変換します。");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputPath)), "UTF-8"));
			PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(outputPath)), "UTF-8")));
			String s = br.readLine(); // 1行目飛ばす
			while((s = br.readLine()) != null){
				Pattern p1 = Pattern.compile("^\\.");
				Pattern p2 = Pattern.compile("曖昧さの回避");
				Pattern p3 = Pattern.compile("^[0-9]{1,100}$");
				Pattern p4 = Pattern.compile("[0-9]{4}.");
				Pattern p5 = Pattern.compile("_\\(");
				Pattern p6 = Pattern.compile("の登場人物");
				Pattern p7 = Pattern.compile("^PJ:");
				Pattern p8 = Pattern.compile("一覧");
				Pattern p9 = Pattern.compile("[*+.,]");
				Matcher m1 = p1.matcher(s);
				Matcher m2 = p2.matcher(s);
				Matcher m3 = p3.matcher(s);
				Matcher m4 = p4.matcher(s);
				Matcher m5 = p5.matcher(s);
				Matcher m6 = p6.matcher(s);
				Matcher m7 = p7.matcher(s);
				Matcher m8 = p8.matcher(s);
				Matcher m9 = p9.matcher(s);
				
				if(!m1.find() && !m2.find() && !m3.find() && !m4.find() && !m5.find() && !m6.find() && !m7.find() && !m8.find() && !m9.find() &&
						s.length() > 3){
					StringBuilder sb = new StringBuilder(s);
					sb.append(",0,0,")
					  .append((int)Math.max(-36000, -400 * Math.pow(s.length(), 1.5)))
					  .append(",名詞,固有名詞,*,*,*,*,")
					  .append(s)
					  .append(",*,*,wikipedia_word,");
					pw.println(sb.toString());
				}
			}
			br.close();
			pw.close();
			System.out.println("完了しました。" + outputPath + "に出力しました。");
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
