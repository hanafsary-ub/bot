package matchabot.twitter;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import matchabot.io.MarkovDictionary;
import matchabot.io.TableInitializer;
import matchabot.parse.EmojiDetector;
import matchabot.parse.MorphologicalAnalysis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class TweetLoader implements Runnable{
	private static final Logger log = LoggerFactory.getLogger( TweetLoader.class );
	private TwitterStream stream;
	
	public TweetLoader(){
		stream = new TwitterStreamFactory().getInstance();
		stream.addListener(new TweetLearner());
		TableInitializer.createMarkovTableIfNeeded();
	}
	@Override
	public void run() {
		stream.sample();
	}
	private class TweetLearner extends StatusAdapter{
		private double learningRate;
		public TweetLearner(){
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream("./input/twitterbot.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			learningRate = Double.parseDouble(prop.getProperty("learning.rate"));
			
		}
		public void onStatus(Status status){
			String message = status.getText();
			if(!status.isRetweet()                             // リツイートでなく
					&& status.getUser().getLang().equals("ja") // 日本語のツイートで
					&& !EmojiDetector.containsEmoji(message)   // 機種依存文字を含んでいなかったら
					&& Math.random() < learningRate            // 一定の確率で学習する
					&& !MarkovDictionary.detectSpam(message)){ // スパムだったら学習しない 

				List<String> morphs = MorphologicalAnalysis.analyze(message);

				int learnings = 0;
				if(isReply(message)){ // リプライメッセージかそうでないか
					learnings = MarkovDictionary.learn(morphs, MarkovDictionary.TABLE_TYPE_REPLY);
				}else{
					learnings = MarkovDictionary.learn(morphs, MarkovDictionary.TABLE_TYPE_TWEET);
				}
				MarkovDictionary.saveTweet(message); // 元のツイート内容も保存する

				// 学習量が1000ツイート突破するごとに標準出力する
				if(learnings % 1000 == 0){
					log.info("学習量" + learnings + "ツイート突破");
				}
			}
		}
		private boolean isReply(String message){
			Pattern p = Pattern.compile("@[A-Za-z0-9_]+[\\s　]?");
			Matcher m = p.matcher(message);
			return m.find();
		}
	}
	public static void main(String[] args){
		new TweetLoader().run();
	}
}
