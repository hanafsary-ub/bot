package matchabot;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import matchabot.io.TableInitializer;
import matchabot.job.QuestionJob;
import matchabot.job.AnswerJob;
import matchabot.job.SubmitJob;
import matchabot.job.ReplyJob;
import matchabot.twitter.TweetLoader;

/**
 * 抹茶SNSBotの起動クラス。
 * @author 花房亮
 *
 */
public class JobRunner {
	private static final Logger log = LoggerFactory.getLogger(JobRunner.class);
	private static String questionSchedule;
	private static String answerSchedule;
	private static String submitSchedule;
	private static String replySchedule;
	static{
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./input/job.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		questionSchedule = prop.getProperty("bot.code.question");
		answerSchedule = prop.getProperty("bot.code.answer");
		submitSchedule = prop.getProperty("bot.twitter.submit");
		replySchedule = prop.getProperty("bot.twitter.reply");
	}
	public static void run() throws SchedulerException{
		TableInitializer.createBotTableIfNeeded(); // もし作成されていなかったらBotのテーブルを作成する
		
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();

		/* 定期出題Jobの登録 */
		JobDetail qjob = newJob(QuestionJob.class).withIdentity("job", "question").build();
		CronTrigger qtrigger = newTrigger().withIdentity("trigger", "question").withSchedule(cronSchedule(questionSchedule)).build();
		sched.scheduleJob(qjob, qtrigger);

		/* 回答者を見つけ次第解答を書き込むJob */
		JobDetail ajob = newJob(AnswerJob.class).withIdentity("job", "answer").build();
		CronTrigger atrigger = newTrigger().withIdentity("trigger", "answer").withSchedule(cronSchedule(answerSchedule)).build();
		sched.scheduleJob(ajob, atrigger);
		
		/* ツイッターから学習するボットの自発的に書き込むJob */
		JobDetail sjob = newJob(SubmitJob.class).withIdentity("job", "submit").build();
		CronTrigger strigger = newTrigger().withIdentity("trigger", "submit").withSchedule(cronSchedule(submitSchedule)).build();
		sched.scheduleJob(sjob, strigger);

		JobDetail rjob = newJob(ReplyJob.class).withIdentity("job", "reply").build();
		CronTrigger rtrigger = newTrigger().withIdentity("trigger", "reply").withSchedule(cronSchedule(replySchedule)).build();
		sched.scheduleJob(rjob, rtrigger);
		
		sched.start();
		log.info("------launched Matcha Bot------");
	}
	public static void main(String[] args) {
		new TweetLoader();
		try {
			JobRunner.run();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
